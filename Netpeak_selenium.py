"""
1. Перейти по ссылке на главную страницу сайта Netpeak. (https://netpeak.ua/).
2. Перейдите на страницу "Работа в Netpeak", нажав на кнопку "Карьера".
3. Перейти на страницу заполнения анкеты, нажав кнопку - "Я хочу работать в Netpeak".
4. Загрузить файл с недопустимым форматом в блоке "Резюме", например png, и проверить что на странице появилось сообщение, о том что формат изображения неверный.
5. Заполнить случайными данными блок "3. Личные данные".
6. Нажать на кнопку отправить резюме.
7. Проверить что сообщение на текущей странице - "Все поля являются обязательными для заполнения" - подсветилось красным цветом.
8. Перейти на страницу "Курсы" нажав соответствующую кнопку в меню и убедиться что открылась нужная страница.
"""

from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time

driver = webdriver.Chrome(executable_path='D://Selenium/chromedriver.exe')
driver.maximize_window()

#_1
driver.get('https://netpeak.ua/')
time.sleep(5)

#_2
btn_karera = driver.find_element_by_xpath('/html/body/header/div[2]/div/div/div[2]/div/nav/ul/li[5]/a').click()
time.sleep(2)

#_3
btn_karera = driver.find_element_by_link_text('Я хочу работать в Netpeak').click()
time.sleep(2)

#_4
driver.find_element_by_name('up_file').send_keys('D://img_01.png')
time.sleep(3)
fail_1 = driver.find_element_by_id('up_file_name').text
print('Уведомление об ошибке_' + fail_1)

#_5
Name = driver.find_element_by_id('inputName').send_keys('Денис')
Lastname = driver.find_element_by_id('inputLastname').send_keys('Белостоцкий')
Email = driver.find_element_by_id('inputEmail').send_keys('denis@belostotskii.com')
Phone = driver.find_element_by_id('inputPhone').send_keys('0999817997')

year = driver.find_element_by_name('by')
y = Select(year)
y.select_by_value('1991')

month = driver.find_element_by_name('bm')
m = Select(month)
m.select_by_value('05')

day = driver.find_element_by_name('bd')
d = Select(day)
d.select_by_value('27')

#_6
otpravit = driver.find_element_by_id('submit').click()
time.sleep(4)

#_7
color1 = driver.find_element_by_class_name('has-error').text
print(color1)
time.sleep(4)

#_8
kyrsi = driver.find_element_by_class_name('blog').click()

centr = driver.find_element_by_xpath('//*[@id="hero"]/div/div/h1').text
print(centr)


time.sleep(3)
driver.quit()